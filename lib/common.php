<?php

const RED = 1;
const GREEN = 2;

$RESULTS = [];
//
function measure($key, callable $callback, $nof = 20) {

	$time = 0.0;
	$mem = 0;

	$mem1 = memory_get_peak_usage(false);
	for($i = 0; $i < $nof; $i++) {
		$start = microtime(true);
		call_user_func($callback);
		$time2 = microtime(true) - $start;
		$time += $time2;
	}
	$mem2 = memory_get_peak_usage(false) - $mem1;
	$mem += $mem2;

	$atime = $time / $nof;
	$amem = $mem / $nof;
	$RESULTS[$key] = [$nof, $atime, $amem];
	e($key . ': ' .
		$nof . ' iterations, ' .
		sprintf('%.6f', $time) . ' seconds, ' .
		'memory taken: ' . $mem . ' bytes', GREEN);
}

function e($string, $kind = 0, $newLine = true) {
	echo
		$kind === 0 ? '' : ("\033[1;3".$kind.'m'),
		$string,
		$kind === 0 ? '' : "\033[0m",
		$newLine ? PHP_EOL : '';
}
