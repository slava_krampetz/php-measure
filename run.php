<?php

require_once __DIR__ . '/lib/common.php';

const MEASURES_PATH = './measures/';

$filter = [];
$has_filter = false;
if ($argc > 1) {
	for($i = 1; $i < $argc; $i++) {
		$arg = $argv[$i];
		if (!preg_match('/^[a-z0-9\-]$/', $arg)) {
			e('Error> incorrect argument "'. $arg . '", should contain only character, digits and -.', RED);
			exit;
		}
		$filter[] = $arg;
		$has_filter = true;
	}
}

$tests = [];
$dh = opendir(MEASURES_PATH);
while( ($file = readdir($dh)) !== false) {
	if( !is_file(MEASURES_PATH . $file))
		continue;
	if(!$has_filter) {
		$tests[] = $file;
	} else {
		foreach($filter as $mask) {
			if (false !== stripos($file, $mask)) {
				$tests[] = $file;
				break;
			}
		}
	}
}
closedir($dh);


if (count($tests) < 1) {
	e('No tests found');
	exit(0);
}

$filter = $argv;

foreach($tests as $script) {
	e('Executing: ' . $script);
	passthru('php -q ' . MEASURES_PATH . $script);
	e('');
}