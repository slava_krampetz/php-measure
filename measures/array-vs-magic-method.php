<?php

require_once __DIR__ . '/../lib/common.php';

const TESTS_NOF = 50000;
const strings_norm = 300;
const strings_from = 100;
const strings_max = 400;

// Data for test
$SESS = [];
$strings = [];

// Correct keys
function str_random($len) {
	$string = 'x';
	while (($len = strlen($string)) < $len) {
		$size = $length - $len;
		$bytes = random_bytes($size);
		$string .= substr(str_replace(['/', '+', '=', "'", '"'], '', base64_encode($bytes)), 0, $size);
	}
	return $string;
}

e('Generating strings...', 0, false);
for($i = 0; $i < strings_max; $i++) {
	$strings[$i][0] = str_random(mt_rand(5, 20));
	$strings[$i][1] = str_random(512);
}
e('. Done.');

class Session {

	public static function set($key, $name) {
		global $SESS;
		return $SESS[$key] = $name;
	}

	public static function &get($key) {
		global $SESS;
		return $SESS[$key];
	}
}

class SessionMagick {

	public function __set($key, $name) {
		global $SESS;
		return $SESS[$key] = $name;
	}

	public function &__get($key) {
		global $SESS;
		return $SESS[$key];
	}
}

echo measure(
	'Global array store ' . strings_norm. ' items',
	function () {
		global $SESS, $strings;
		for ($i = 0; $i < strings_norm; ++$i) {
			$key = $strings[$i][0];
			$val = $strings[$i][1];
			$SESS[$key] = $val;
		}
	},
	TESTS_NOF
);

echo measure(
	'Wrapper class, static method, store ' . strings_norm. ' items',
	function () {
		global $strings;
		for ($i = 0; $i < strings_norm; ++$i) {
			$key = $strings[$i][0];
			$val = $strings[$i][1];
			Session::set($key, $val);
		}
	},
	TESTS_NOF
);

echo measure(
	'Wrapper class, magick method, store ' . strings_norm. ' items',
	function () {
		global $strings;
		$sess = new SessionMagick();
		for ($i = 0; $i < strings_norm; ++$i) {
			$key = $strings[$i][0];
			$val = $strings[$i][1];
			$sess->$key = $val;
		}
	},
	TESTS_NOF
);

echo measure(
	'Global array store read ' . strings_norm. ' items (' .
	(strings_norm - strings_from) .	'/' . (strings_max - strings_norm) . ')',
	function () {
		global $SESS, $strings;
		for ($i = strings_from; $i < strings_max; ++$i) {
			$key = $strings[$i][0];
			$val = $SESS[$key];
		}
	},
	TESTS_NOF
);

echo measure(
	'Wrapper class, static method, read ' . strings_norm. ' items (' .
		(strings_norm - strings_from) .	'/' . (strings_max - strings_norm) . ')',
	function () {
		global $strings;
		for ($i = strings_from; $i < strings_max; ++$i) {
			$key = $strings[$i][0];
			$val = Session::get($key);
		}
	},
	TESTS_NOF
);


echo measure(
	'Wrapper class, magick method, read ' . strings_norm. ' items (' .
		(strings_norm - strings_from) .	'/' . (strings_max - strings_norm) . ')',
	function () {
		global $strings;
		$sess = new SessionMagick();
		for ($i = strings_from; $i < strings_max; ++$i) {
			$key = $strings[$i][0];
			$val = $sess->$key;
		}
	},
	TESTS_NOF
);


